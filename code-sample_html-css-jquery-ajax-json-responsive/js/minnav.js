(function ($, window, i) {
	$.fn.minNav = function (options) {
		
		// default settings
		var settings = $.extend({
			'active' : 'selected', // set the "active" class
			'header' : '', // specify text for "header" and show header instead of the active item
			'label'  : '' // sets the <label> text for the <select> (if not set, no label will be added)
		}, options);
		
		return this.each(function () {
			i++;
			
			var $nav = $(this),
				namespace = 'minnav',
				namespace_i = namespace + i,
				l_namespace_i = '.l_' + namespace_i,
				$select = $('<select/>').attr("id", namespace_i).addClass(namespace + ' ' + namespace_i);
			
			if ($nav.is('ul,ol')) {
				
				if (settings.header !== '') {
					$select.append($('<option/>').text(settings.header));
				}
				
				// build options
				var options = '';
				
				$nav.addClass('l_' + namespace_i).find('a').each(function () {
					options += '<option value="' + $(this).attr('href') + '">';
					for (var j=0; j<$(this).parents('ul, ol').length-1; j++) {
						options += '- ';
					}
					options += $(this).text() + '</option>';
				});
				
				// append options into a select
				$select.append(options);
				
				// select the active item
				if (!settings.header) {
					$select.find(':eq(' + $(l_namespace_i + ' li').index($(l_namespace_i + ' li.' + settings.active)) + ')').attr('selected', true);
				}
				
				// change window location
				$select.change(function () {
					window.location.href = $(this).val();
				});
				
				// inject select
				$(l_namespace_i).after($select);
				
				// inject label
				if (settings.label) {
					$select.before($("<label/>").attr("for", namespace_i).addClass(namespace + '_label ' + namespace_i + '_label').append(settings.label));
				}
			
			}
		});
	};
})(jQuery, this, 0);
