// remap jQuery to $
(function($) {

	// content is ready
	$(document).ready(function () {
		// hide #back-top first
		$("#back-top").hide();
		
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					$('#back-top').fadeIn();
				}
				else {
					$('#back-top').fadeOut();
				}
			});
	
			// scroll body to 0px on click
			$('#back-top a').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});
		
		/* sidebar images
		 ---------------------------------------------------------------------- */
		$(function () {
			var title = $('#sidebar-title');
			var container = $('#sidebar-images');
			var count = container.data('image-count');
			$.ajax({
				url: 'images.json',
				dataType: 'json',
				timeout: 10000,
				type: 'GET',
				error:
					function(xhr, status, error) {
						container.html('An error occured: ' + error);
					},
				success:
					function(data, status, xhr) {
						var i=1;
						title.html(data.title);
						$.each(data.images, function(index, element) {
							container.append('<img src="' + element.file + '" title="' + element.title + ' - ' + i + '" alt="' + element.title + '" />');
							if(i%count == 0) {
								return false; //break each loop if counter matches 'data-image-count' attribute
							}
							else {
								i++;
							}
						});
					}
			});
		});
		
	});
	
	// sticky header with resize
	window.onscroll = function () {
		var top = window.pageXOffset ? window.pageXOffset : document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
		if(top > 50) {
			$("#header").css("position", "fixed");
			document.getElementById("header").style.height = "50px";
			document.getElementById("logo-img").style.height = "50px";
			$("#top-nav").addClass("hidden");
			$("select#minnav1").css("top", "1px");
			$("select#minnav2").css("top", "26px");
		}
		else {
			$("#header").css("position", "absolute");
			document.getElementById("header").style.height = "100px";
			document.getElementById("logo-img").style.height = "100px";
			$("#top-nav").removeClass("hidden");
			$("select#minnav1").css("top", "18px");
			$("select#minnav2").css("top", "58px");
		}
	}

})(window.jQuery);