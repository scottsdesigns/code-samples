<?php $config = include('../includes/config.php'); ?>
<?php include('../includes/tracks.php'); ?>
<?php include('../includes/functions.php'); ?>
<?php

$page_query_string_array = array();
$alpha_query_string_array = array();
$order_query_string_array = array();

if(isset($_POST['page'])) {
	$page = (int)$_POST['page'];
	$order_query_string_array['page'] = $_POST['page'];
}
else {
	$page = 1;
}

if(isset($_POST['order'])) {
	if($_POST['order'] == 'asc') {
		$order = $_POST['order'];
		$order_str = 'tr.name ASC';
		$page_query_string_array['order'] = 'asc';
		$alpha_query_string_array['order'] = 'asc';
	}
	elseif($_POST['order'] == 'desc') {
		$order = $_POST['order'];
		$order_str = 'tr.name DESC';
		$page_query_string_array['order'] = 'desc';
		$alpha_query_string_array['order'] = 'desc';
	}
	elseif($_POST['order'] == 'rand') {
		$order = $_POST['order'];
		$order_str = 'RAND()';
		$page_query_string_array['order'] = 'rand';
		$alpha_query_string_array['order'] = 'rand';
	}
	else {
		$order = '';
		$order_str = 'tr.name ASC';
	}
}
else {
	$order = '';
	$order_str = 'tr.name ASC';
}

if(isset($_POST['alpha'])) {
	$alpha = $_POST['alpha'];
	$page_query_string_array['alpha'] = $_POST['alpha'];
	$order_query_string_array['alpha'] = $_POST['alpha'];
}
else {
	$alpha = '';
}


$page_query_string = http_build_query($page_query_string_array);
$alpha_query_string = http_build_query($alpha_query_string_array);
$order_query_string = http_build_query($order_query_string_array);

$limit = 11;
$result = getTrackActivity($limit,$order_str,$page,$alpha); //default is 10, tr.name ASC, 1, 'a'
$tracks_count = mysql_num_rows($result);
$tracks_total = getTrackActivityCount($alpha);

$last_page = ceil($tracks_total/$limit);
if($tracks_count > 0) {
	$tracks_floor = $page*$limit-$limit+1;
}
else {
	$tracks_floor = 0;
}
$tracks_ceil = $page*$limit-$limit+$tracks_count;

$track_count = $tracks_floor;

$current = $page;
$pages = $last_page;
$links = array();

//unset($query_string_array['page']);
//$page_query_string = http_build_query($query_string_array);

if ($pages > 3) {
    // this specifies the range of pages we want to show in the middle
    $min = max($current - 2, 2);
    $max = min($current + 2, $pages-1);

    // we always show the first page
    $links[] = '<li'.($current == 1 ?  ' class="active"' : '').'>'.($current == 1 ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page=1' : '?page=1').'">').'1'.($current == 1 ?  '</span>' : '</a>').'</li>';

    // we're more than one space away from the beginning, so we need a separator
    if ($min > 2) {
        $links[] = '<li><span>...</span></li>';
    }

    // generate the middle numbers
    for ($i=$min; $i<$max+1; $i++) {
        $links[] = '<li'.($current == $i ?  ' class="active"' : '').'>'.($current == $i ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page='.$i : '?page='.$i).'">').$i.($current == $i ?  '</span>' : '</a>').'</li>';
    }

    // we're more than one space away from the end, so we need a separator
    if ($max < $pages-1) {
        $links[] = '<li><span>...</span></li>';
    }
    // we always show the last page
    $links[] = '<li'.($current == $pages ?  ' class="active"' : '').'>'.($current == $pages ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page='.$pages : '?page='.$pages).'">').$pages.($current == $pages ?  '</span>' : '</a>').'</li>';
}
elseif($pages == 3) {
    // we must special-case three or less, because the above logic won't work
    $links = array('<li'.($current == 1 ?  ' class="active"' : '').'>'.($current == 1 ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page=1' : '?page=1').'">').'1'.($current == 1 ?  '</span>' : '</a>').'</li>', '<li'.($current == 2 ?  ' class="active"' : '').'>'.($current == 2 ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page=2' : '?page=2').'">').'2'.($current == 2 ?  '</span>' : '</a>').'</li>', '<li'.($current == 3 ?  ' class="active"' : '').'>'.($current == 3 ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page=3' : '?page=3').'">').'3'.($current == 3 ?  '</span>' : '</a>').'</li>');
}
elseif($pages == 2) {
    // we must special-case three or less, because the above logic won't work
    $links = array('<li'.($current == 1 ?  ' class="active"' : '').'>'.($current == 1 ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page=1' : '?page=1').'">').'1'.($current == 1 ?  '</span>' : '</a>').'</li>', '<li'.($current == 2 ?  ' class="active"' : '').'>'.($current == 2 ?  '<span>' : '<a href="#!/pages/tracks'.(!empty($page_query_string) ? '?'.$page_query_string.'&page=2' : '?page=2').'">').'2'.($current == 2 ?  '</span>' : '</a>').'</li>');
}
else {
    // we must special-case three or less, because the above logic won't work
    $links = array('<li'.($current == 1 ?  ' class="active"' : '').'><span>1</span></li>');
}

?>
<!-- ############################# Ajax Page Container ############################# -->
<section id="page" data-title="<?php echo $config['params']['title']; ?> |Track Stats">

	<!-- ############################# Intro ############################# -->
	<section class="intro-title section border-bottom" style="background-image: url(placeholders/releases-bg.jpg)">
		<h1 class="heading-l">Music <span class="color">Charts</span></h1>
	</section>
	<!-- /intro -->

	<!-- ############################# Content ############################# -->
	<section class="content section">
		<!-- container -->
		<div class="container">
        	
            <header class="section-header">
				<h2 class="section-title">Top Song Plays</h2>
			</header>
            
            <ul id="release-list" class="tracklist">
                <?php
				$pl_num = 1;
				while ($row = mysql_fetch_assoc($result)) {
					$track_duration = formatRoundedSeconds($row['track_duration']);
					$artists = getTrackArtists($row['artist_ids']);
					$artist_array = array();
					$artist_link_array = array();
					while ($artist = mysql_fetch_assoc($artists)) {
						$artist_array[] = $artist['artist_name'];
						$artist_link_array[] = '<a href="#!/pages/artist?id='.$artist['artist_id'].'&name='.cleanUrl($artist['artist_name'], '_', false).'" title="'.$artist['artist_name'].'" class="lighter">'.$artist['artist_name'].'</a>';
					}
				?>
                <li>
                    <div class="track-details track-details-80">
                        <!-- left side -->
                        <span class="track-num track-num-80"><?php echo number_format($track_count); ?></span>
                        <!-- /left side -->
                        
                        <!-- right side -->
                        <div class="track">
                            
                            <!-- cover -->
                            <a class="sp-play-track" href="<?php echo $config['params']['music_root'].'/'.$row['track_file']; ?>" data-tracking_id="<?php echo $row['track_id']; ?>" data-cover="<?php echo $config['params']['image_root'].'/'.getCoverThumb($row['track_cover'], '110'); ?>" title="<?php echo $row['track_name']; ?>">
                            	<img class="track-cover" src="<?php echo $config['params']['image_root'].'/'.getCoverThumb($row['track_cover'], '110'); ?>" title="<?php echo $row['track_name']; ?>" alt="<?php echo $row['track_name']; ?>">
                                <span class="status-overlay"></span>
                                <span style="display:none;" class="track-title"><?php echo $row['track_name']; ?></span>
                                <span style="display:none;" class="track-artists">by <?php echo implode(', ', $artist_array); ?> from <?php echo $row['album_name']; ?></span>
                            </a>
                            <!-- Title -->
                            <a class="sp-play-track" href="<?php echo $config['params']['music_root'].'/'.$row['track_file']; ?>" data-tracking_id="<?php echo $row['track_id']; ?>" data-cover="<?php echo $config['params']['image_root'].'/'.getCoverThumb($row['track_cover'], '110'); ?>" title="<?php echo $row['track_name']; ?>">
                            	<span class="track-title activity"><?php echo $row['track_name']; ?></span>
                                <span style="display:none;" class="track-artists">by <?php echo implode(', ', $artist_array); ?> from <?php echo $row['album_name']; ?></span>
                            </a>
                            <!-- Artists -->
                            <span class="track-artists">
								<?php echo number_format((int)$row['track_action_count']); ?> play<?php echo (int)$row['track_action_count'] == 1 ? '' : 's'; ?> (<?php echo $track_duration; ?>) by <?php echo implode(', ', $artist_link_array); ?>
                                from <a href="#!/pages/album?id=<?php echo $row['album_id']; ?>&name=<?php echo cleanUrl($row['album_name'], '_', false); ?>&by=<?php echo cleanUrl(implode(', ', $artist_array), '_', false); ?>" title="<?php echo $row['album_name']; ?>" class="lighter"><?php echo $row['album_name']; ?></a>
								<?php /*while ($artist = mysql_fetch_assoc($artists)) { ?>
                                <?php echo '<span class="lighter">'.$row['album_name'].'</span> by <span class="lighter">'.$artist['artist_name'].'</span>'; ?>
                                <?php }*/ ?>
                            </span>
							
                        </div>

                        <div class="track-buttons">
                            
                            <a class="track sp-add-track" href="<?php echo $config['params']['music_root'].'/'.$row['track_file']; ?>" data-tracking_id="<?php echo $row['track_id']; ?>" data-cover="<?php echo $config['params']['image_root'].'/'.getCoverThumb($row['track_cover'], '110'); ?>" title="Add to Playlist">
                            	<img src="img/playlist_24x24_grey.png" />
                                <span style="display:none;" class="track-title"><?php echo $row['track_name']; ?></span>
                                <span style="display:none;" class="track-artists">by <?php echo implode(', ', $artist_array); ?> from <?php echo $row['album_name']; ?></span>
                            </a>
                            <a class="track sp-play-track" href="<?php echo $config['params']['music_root'].'/'.$row['track_file']; ?>" data-tracking_id="<?php echo $row['track_id']; ?>" data-cover="<?php echo $config['params']['image_root'].'/'.getCoverThumb($row['track_cover'], '110'); ?>" title="Play Now">
                            	<img src="img/play_24x24_grey.png" />
                                <span style="display:none;" class="track-title"><?php echo $row['track_name']; ?></span>
                                <span style="display:none;" class="track-artists">by <?php echo implode(', ', $artist_array); ?> from <?php echo $row['album_name']; ?></span>
                            </a>
                            
                        </div>
                        <!-- /right side -->
                    </div>
                </li>
				
                <?php $pl_num++; $track_count++; } ?>
            </ul>
        
        </div>
    </section>
</section>