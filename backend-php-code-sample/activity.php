<?php
// set song (track) play activity posted via ajax

if($_SERVER['SERVER_NAME'] == 'localhost') {
	$mysqli = new mysqli('localhost', 'root', '', 'mpm_noyii');
}
else {
	$mysqli = new mysqli('localhost', 'database_user', 'database_pass', 'database_name'); //removed the real information for obvious reasons
}

$result = array();

// check connection error
if(mysqli_connect_errno()) {
	$result['error'] = true;
	$result['msg'] = mysqli_connect_error();
	$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	echo $return;
	exit();
}

// create prepared statement
if($stmt = $mysqli->prepare("INSERT INTO `track_activity` (`track_id`, `action_id`, `ip`, `timestamp`) VALUES (?, ?, ?, NOW())")) {
	// bind params
	$stmt->bind_param('iis', $track_id, $action_id, $ip);
	// i = corresponding variable has type integer
	// d = corresponding variable has type double
	// s = corresponding variable has type string
	// b = corresponding variable is a blob and will be sent in packets
	
	// set params
	$track_id = $_POST['track_id'];
	$action_id = $_POST['action_id'];
	$ip = $_SERVER['REMOTE_ADDR'];
	
	// execute prepared Statement
	$stmt->execute();
	
	// results
	$result['success'] = true;
	$result['msg'] = 'Tracked '.$_POST['track_id'].' successfully';
	
	// close statement
	$stmt->close();
}
else {
	// error
	$result['error'] = true;
	$result['msg'] = $mysqli->error;
}

$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
echo $return;
exit();
?>